# M2 Internship - Nanopelagicales

## Study of the Pangenomic Diversity of the Order Nanopelagicales in Freshwater Lake Ecosystems and the Influence of Environmental Parameters

#### Author : ARQUE Maxime - Master 2 Bio-informatique

#### Date : 06/06/2024

The diversity and distribution of microbial communities, as well as their functions in
lacustrine ecosystems, are less studied than their counterparts in soils or oceans, although
these microorganisms play a crucial role in biogeochemical cycles and trophic interactions. In
this context, the Nanopelagicales (acI), an order of freshwater bacteria, stand out for their
prevalence and functional diversity. We characterized the taxonomic diversity of
Nanopelagicales, focusing on the acI-A, acI-B, and acI-C clades. We also examined their
relationships with different environmental parameters such as temperature, pH, oxygen, and
their distribution in the studied lakes. Finally, we adopted a pangenomic approach to gain
insight into the genomic diversity at the order and clade levels of Nanopelagicales, in order to
analyze the pathways and functional capacities involved in distinct ecological roles. The
study refined the characterization of the clades, notably acI-C. We identified tribe
distributions according to environmental parameters such as pH, with acI-B1 found in
alkaline lakes and acI-B2 found in acidic lakes. However, the study of functional capacities
involved in distinct ecological roles requires further attention before drawing conclusions.


This internship focused on studying the diversity of the order Nanopelagicales using data from metagenome-assembled genomes (MAGs) from lakes around the globe. Firstly, I characterized the taxonomic diversity of the order Nanopelagicales, specifically focusing on the acI-A, acI-B, and acI-C clades. Secondly, I evaluated the potential relationships between various environmental parameters (temperature, pH, oxygen, geography, climate) and the distribution of the clades within the different lakes considered. Thirdly, I conducted a pangenomic study to obtain an overview of the genomic diversity both at the order level of Nanopelagicales and within the different clades. To study the differentiation of these clades at the genomic and functional levels, the pathways and functional capabilities that could lead to distinct roles in the ecosystem were considered.


