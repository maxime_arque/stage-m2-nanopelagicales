#!/bin/bash

################################################################################
#                                                                              #
#                              Anvio Analysis Script                           #
#                                                                              #
# Author       : Maxime Arque                                                  #
# Date         : February 01, 2024                                             #
# Description  : This script performs genome analysis using Anvio. It includes #
#                steps for creating pangenomes and conducting pangenomic       #
#                analysis.                                                     #
#                                                                              #
# Usage        : Submit this script as a SLURM job using the sbatch command.   #
#                Ensure that the necessary directories and dependencies are    #
#                set up before execution.                                      #
#                                                                              #
# SLURM Options:                                                               #
#   --time          : Maximum runtime (24 hours)                               #
#   --ntasks        : Number of tasks (1)                                      #
#   --mem           : Memory allocation (100G)                                 #
#   --cpus-per-task : Number of CPUs per task (64)                             #
#   -o              : Standard output log directory                            #
#   -e              : Standard error log directory                             #
#   --mail-user     : Email for job notifications                              #
#   --job-name      : Job name (Anvio_pang)                                    #
#   --partition     : SLURM partition (lmge)                                   #
#                                                                              #
################################################################################

#SBATCH --time=24:00:00 
#SBATCH --ntasks=1
#SBATCH --mem=100G
#SBATCH --cpus-per-task=64
#SBATCH -o log/slurmjob-%A-%a.out
#SBATCH -e log/slurmjob-%A-%a.err
#SBATCH --mail-user=maxime.arque@etu.uca.fr
#SBATCH --job-name=Anvio_pang
#SBATCH --partition=lmge

echo 'Anvio analysis on all genomes'

# Upload whatever required packages
module load conda/23.3.1
eval "$(conda shell.bash hook)"
source activate anvio-8

echo "Set up directories ..."
# Set up data and result directories
DATA_DIR="$HOME/data/anvio"
OUTPUT_DIR="$HOME/data/anvio"

SCRATCHDIR=/storage/scratch/"$USER"/${SLURM_JOB_ID}
mkdir -p -m 700 "$SCRATCHDIR"

TMPDIR=$SCRATCHDIR/tmp
mkdir $TMPDIR

if [ ! -d "$DATA_DIR/PANGENOMICS" ]; then
    mkdir "$DATA_DIR/PANGENOMICS"
fi

if [ ! -d "$SCRATCHDIR/PANGENOMICS" ]; then
    mkdir "$SCRATCHDIR/PANGENOMICS"
fi

echo "~~~~~~~~~ Creation of the genome list file... ~~~~~~~~~"
files=$(find $DATA_DIR/CONTIGS -type f -name *.db)

echo -e "name\tcontigs_db_path" > $SCRATCHDIR/PANGENOMICS/list_genome.tsv

for file in $files
do
    name=$(echo $file | grep -oh "\w*.db" | sed 's/_contigs\.db//')
    echo -e "$name\t$file" >> $SCRATCHDIR/PANGENOMICS/list_genome.tsv
done

echo "~~~~~~~~~ Creation of the genome storage database... ~~~~~~~~~"
anvi-gen-genomes-storage -e $SCRATCHDIR/PANGENOMICS/list_genome.tsv \
    -o $SCRATCHDIR/PANGENOMICS/nanopelagicales-GENOMES.db

echo "~~~~~~~~~ PANGENOME Analysis... ~~~~~~~~~"
anvi-pan-genome -g $SCRATCHDIR/PANGENOMICS/nanopelagicales-GENOMES.db \
    -o $SCRATCHDIR/PANGENOMICS -n nanopelagicales -T ${SLURM_CPUS_PER_TASK} \
    --enforce-hierarchical-clustering --I-know-this-is-not-a-good-idea

echo "~~~~~~~~~ Cleaning the directory... ~~~~~~~~~"
over_data=$(find $SCRATCHDIR/PANGENOMICS -type f ! -name "*.db")
if [ ! -d "$SCRATCHDIR/PANGENOMICS/additional_files" ]; then
    mkdir "$SCRATCHDIR/PANGENOMICS/additional_files"
fi

mv $over_data $SCRATCHDIR/PANGENOMICS/additional_files
rm -rf $SCRATCHDIR/PANGENOMICS/additional_files

# Moving scratchdir files to output files
mv $SCRATCHDIR/PANGENOMICS/* $OUTPUT_DIR/PANGENOMICS

rm -rf "$SCRATCHDIR"

echo "Stop job : $(date)"


