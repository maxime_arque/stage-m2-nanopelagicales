library(FactoMineR)
library(factoextra)
library(ade4)
library(dplyr)
library(explor)

metadata=read.table("/home/maxarque/Bureau/metadata_pavin/only_freshwater_nanopelagicaceae_V3.tsv", header=T, sep="\t")
temp=metadata[which(!is.na(metadata$temperature)),]
temp=temp$temperature
quantile(temp, probs = c(0.25,0.5,0.75,1)) 

metadata$layer_oxygen=ifelse(metadata$layer_oxygen == "moyennement oxygéné", "sub-oxique", metadata$layer_oxygen)


get_layer_temperature = function(oxygen, temperature, ph) {
  if (!is.na(temperature)) {
    if (temperature < 5) {
      return("froid")
    } else if (temperature > 15) {
      return("chaud")
    } else {
      return("tempéré")
    }
  } else {
    return(NA)
  }
}

metadata$layer_temperature = ifelse(!is.na(metadata$temperature), 
                                    sapply(1:nrow(metadata), function(i) {
                                      get_layer_temperature(metadata$oxygen[i], metadata$temperature[i], metadata$ph[i])
                                    }), NA)




metadata$lake_thermal_region=ifelse(metadata$lake_thermal_region=="NF", "NF - Northern Frigid",
                                    ifelse(metadata$lake_thermal_region=="NC", "NC - Northern Cool",
                                           ifelse(metadata$lake_thermal_region=="NT", "NT - Northern Temperate",
                                                  ifelse(metadata$lake_thermal_region=="NW", "NW - Northern Warm",
                                                         ifelse(metadata$lake_thermal_region=="NH", "NH - Northern Hot",
                                                                ifelse(metadata$lake_thermal_region=="TH", "TH - Tropical Hot",
                                                                       ifelse(metadata$lake_thermal_region=="SH", "SH - Southern Hot",
                                                                              ifelse(metadata$lake_thermal_region=="SW", "SW - Southern Warm",
                                                                                     ifelse(metadata$lake_thermal_region=="ST", "ST - Southern temperate",NA)))))))))



oxygen=as.matrix.data.frame(table(metadata$layer_oxygen, metadata$clean_exp))
rownames(oxygen)=rownames(table(metadata$layer_oxygen, metadata$clean_exp))
colnames(oxygen)=colnames(table(metadata$layer_oxygen, metadata$clean_exp))



temperature=as.matrix.data.frame(table(metadata$layer_temperature, metadata$clean_exp))
rownames(temperature)=rownames(table(metadata$layer_temperature, metadata$clean_exp))
colnames(temperature)=colnames(table(metadata$layer_temperature, metadata$clean_exp))
temperature=temperature[c(2,3,1),]

ph=as.matrix.data.frame(table(metadata$layer_ph, metadata$clean_exp))
rownames(ph)=rownames(table(metadata$layer_ph, metadata$clean_exp))
colnames(ph)=colnames(table(metadata$layer_ph, metadata$clean_exp))
ph=ph[c(1,3,2),]


##### TESTS STATISTIQUES

oxy=chisq.test(oxygen)
stdres=oxy$stdres
result_oxy=dnorm(stdres)
result_oxy=as.data.frame(p.adjust(result_oxy, method="BH", n = length(result_oxy)))
result_oxy=as.data.frame(matrix(result_oxy$`p.adjust(result_oxy, method = "BH", n = length(result_oxy))`, nrow=3, ncol=22, byrow = FALSE))
colnames(result_oxy)=colnames(oxygen)
rownames(result_oxy)=rownames(oxygen)


temp=chisq.test(temperature)
stdres=temp$stdres
result_temp=dnorm(stdres)
result_temp=as.data.frame(p.adjust(result_temp, method="BH", n = length(result_temp)))
result_temp=as.data.frame(matrix(result_temp$`p.adjust(result_temp, method = "BH", n = length(result_temp))`, nrow=3, ncol=22, byrow = FALSE))
colnames(result_temp)=colnames(temperature)
rownames(result_temp)=rownames(temperature)

phph=chisq.test(ph)
stdres=phph$stdres
result_ph=dnorm(stdres)
result_ph=as.data.frame(p.adjust(result_ph, method="BH", n = length(result_ph)))
result_ph=as.data.frame(matrix(result_ph$`p.adjust(result_ph, method = "BH", n = length(result_ph))`, nrow=3, ncol=22, byrow = FALSE))
colnames(result_ph)=colnames(ph)
rownames(result_ph)=rownames(ph)

metadata$geographic_location_depth=as.numeric(metadata$geographic_location_depth)

metadata=metadata %>%
  mutate(region_2 = case_when(
    region == "Besse-et-Saint-Anastaise, Lac Pavin" & geographic_location_depth > 60 ~ "Lac Pavin_monimolimnion",
    region == "Besse-et-Saint-Anastaise, Lac Pavin" & geographic_location_depth  <= 60 ~ "Lac Pavin_mixolimnion",
    TRUE ~ region
  ))

metadata$region_2=gsub("Lake ", "", metadata$region_2)
metadata$region_2=gsub("Lac ", "", metadata$region_2)



###### STATS ACM : Analyse des correspondances multiples

#data=metadata[,c("clean_exp","layer_oxygen","layer_temperature","layer_ph","region","geographic_location","season","lake_thermal_region")]
data=metadata[,c("clean_exp","layer_oxygen","layer_temperature","layer_ph","region_2","geographic_location","season","lake_thermal_region")]
data=metadata[,c("clean_exp","layer_oxygen","layer_temperature","layer_ph","region_2","lake_thermal_region")]


data=data[which(!is.na(data$layer_oxygen) & !is.na(data$layer_temperature) & !is.na(data$layer_ph)),]

data=data[-which(data$clean_exp=="acI-A*1"),]
data=data[-which(data$clean_exp=="acI-A*3"),]
data=data[-which(data$clean_exp=="acI-A*4"),]
data=data[-which(data$clean_exp=="CAIMZW01"),]
data=data[-which(data$clean_exp=="GCA-2737595"),]
data=data[-which(data$clean_exp=="QYPT01"),]
data=data[-which(data$clean_exp=="None"),]
data=data[-which(data$clean_exp=="WLQV01"),]
data=data[-which(data$clean_exp=="acI-A*"),]
#data=data[-which(data$clean_exp=="acI-A*7"),]
#data=data[-which(data$clean_exp=="acI-A*6"),]
#data=data[-which(data$clean_exp=="acI-A*5"),]



data$clean_exp=as.factor(data$clean_exp)
data$layer_oxygen=as.factor(data$layer_oxygen)
data$layer_temperature=as.factor(data$layer_temperature)
data$layer_ph=as.factor(data$layer_ph)

#data$region=as.factor(data$region)
data$region_2=as.factor(data$region_2)

#data$season=as.factor(data$season)
#data$geographic_location=as.factor(data$geographic_location)
data$lake_thermal_region=as.factor(data$lake_thermal_region)


acm=dudi.acm(data, scan=F)
#barplot(acm$eig)



scatter(acm, col = rep(c("black", "red3", "darkblue","orange","green","red1","purple","bisque4","darkgray","cyan","gold","darkseagreen"), 2))


library(explor)

res=explor::prepare_results(acm)

explor::MCA_biplot(res, xax = 1, yax = 2, col_var = "Variable", ind_point_size = 32,
                   ind_opacity = 0.5, ind_opacity_var = NULL, ind_labels = FALSE, var_point_size = 96,
                   var_sup = FALSE, ind_sup = FALSE, labels_size = 12, bi_lab_min_contrib = 0,
                   symbol_var = "Nature", transitions = TRUE, labels_positions = NULL, xlim = c(-4.21,
                                                                                                2.31), ylim = c(-3.6, 2.92))

library(cowplot)
library(Polychrome)

res.mca=MCA(data, graph=F)
eig.val=get_eigenvalue(res.mca)



model="norm"
level=0.65
pal=c("dodgerblue2", "#E31A1C", "green4", "#6A3D9A", "#FF7F00", "black", "gold1",
      "skyblue2", "palegreen2", "#FDBF6F", "gray70", "maroon", "orchid1", "darkturquoise", 
      "darkorange4", "brown")

pal=unname(createPalette(15,  c("#ff0000", "#00ff00", "#0000ff")))

axis=c(1,4)
axis=c(1,2)

clean_exp=fviz_mca_ind(res.mca, label = "none", habillage = "clean_exp", pointsize=1,
                       palette = pal, axes = axis,
                       addEllipses = TRUE, ellipse.type = model, title="Classification Newtonienne", ellipse.level=level,
                       ggtheme = theme_bw()) + theme(plot.title = element_text(hjust = 0.5))


lake_thermal_region=fviz_mca_ind(res.mca, label = "none", habillage = "lake_thermal_region", pointsize=1,
                                 palette = c("#4575b4","#666666","#74add1","#ffd92f","#e78ac3","#e41a1c"), axes = axis,
                                 addEllipses = TRUE, ellipse.type = model, title="Région thermique", ellipse.level=level,
                                 ggtheme = theme_bw()) + theme(plot.title = element_text(hjust = 0.5))


layer_oxygen=fviz_mca_ind(res.mca, label = "none", habillage = "layer_oxygen", pointsize=1,
                          palette = c("#F21212","#12E4F2","#126EF2"), axes = axis,
                          addEllipses = TRUE, ellipse.type = model, title="Oxygène", ellipse.level=level,
                          ggtheme = theme_bw()) + theme(plot.title = element_text(hjust = 0.5))



layer_ph=fviz_mca_ind(res.mca, label = "none", habillage = "layer_ph", pointsize=1,
                      palette = c("#0C3DB8","#F21212","#3CA215"), axes = axis,
                      addEllipses = TRUE, ellipse.type = model, title="pH", ellipse.level=level,
                      ggtheme = theme_bw()) + theme(plot.title = element_text(hjust = 0.5))


layer_temperature=fviz_mca_ind(res.mca, label = "none", habillage = "layer_temperature", pointsize=1,
                               palette = c("#F21212","#0C3DB8","#F2B212"), axes = axis,
                               addEllipses = TRUE, ellipse.type = model, title="Température", ellipse.level=level,
                               ggtheme = theme_bw()) + theme(plot.title = element_text(hjust = 0.5))


region_2=fviz_mca_ind(res.mca, label = "none", habillage = "region_2", pointsize=1,
                      palette = pal, axes = axis,
                      addEllipses = TRUE, ellipse.type = model, title ="Région / lac", ellipse.level=level,
                      ggtheme = theme_bw()) + theme(plot.title = element_text(hjust = 0.5))


cowplot::plot_grid(clean_exp,
                   layer_oxygen,
                   layer_temperature,
                   layer_ph,
                   region_2,
                   lake_thermal_region,
                   ncol=3, nrow=2, 
                   labels = c("A","B","C","D","E","F"))

plot=cowplot::plot_grid(clean_exp,
                        layer_oxygen,
                        layer_temperature,
                        layer_ph,
                        region_2,
                        lake_thermal_region,
                        ncol=2, nrow=3, 
                        labels = c("A","B","C","D","E","F"))

ggsave2("/home/maxarque/Bureau/metadata_pavin/AMC_2.svg", plot, limitsize = T, width = 10, height = 13)



#fviz_contrib(acm, choice = "var", axes = 1)
#fviz_contrib(acm, choice = "var", axes = 2)
#fviz_contrib(acm, choice = "var", axes = 3)


